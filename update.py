import time
import os
from selenium import webdriver


def get_chrome_options():
    options = webdriver.ChromeOptions()
    options.headless = True
    options.add_argument('window-size=1024,768')
    options.add_argument('--no-sandbox')
    return options

f2 = open('./public/index.html', 'a')

with open('list.txt') as f:
    with webdriver.Chrome(options=get_chrome_options()) as driver:
        for line in f:
            line = line.strip()
            url = 'https://'+line+'.հայ'
            driver.get(url)
            time.sleep(3)
            driver.save_screenshot(line+'.png')
            os.system('cwebp -z 9 ' + line + '.png -o ./public/' + line + '.webp')
            s = '<a href="'+url+'"><img src="'+line+'.webp" alt="' +line+'.հայ" /></a>\n'
            print(s)
            f2.write(s)

f2.write('</section></body></html>')
f2.close()
